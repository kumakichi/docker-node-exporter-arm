#!/bin/bash
#
# Description: Expose Odroid CPU temperature 
#
# Author: m.levec@levigo.de
CPUTEMP=$(</sys/devices/virtual/thermal/thermal_zone0/temp)
echo '# HELP node_cpu_temp_total the cpu core temperature on an odroid'
echo '# TYPE node_cpu_temp_total gauge'
echo 'node_cpu_temp_total ' $CPUTEMP
