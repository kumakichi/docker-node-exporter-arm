#https://www.balena.io/docs/reference/base-images/base-images/#balena-base-images?ref=dockerhub
# switch back to official binary release as compilation fails with index out of bounds when parsing

FROM arm32v7/alpine:3.16.2
RUN apk --update add ca-certificates curl wget \
    && mkdir -p /tmp/install /bin \
    && wget -O /tmp/install/node_exporter.tar.gz https://github.com/prometheus/node_exporter/releases/download/v1.3.1/node_exporter-1.3.1.linux-armv7.tar.gz \  
    && cd /tmp/install \ 
    && tar vxf /tmp/install/node_exporter.tar.gz \ 
    && cp /tmp/install/node_exporter-1.3.1.linux-armv7/node_exporter /bin 

ENV VERSION 1.3.1 

EXPOSE     9100
ENTRYPOINT [ "/bin/node_exporter" ]


