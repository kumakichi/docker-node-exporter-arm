## Requirements
Intended for use on ARM32v6 or ARM32v7. Can be adapted to amd64 or others where docker is available.
You will need docker-ce > 18.x and for more fun, docker-compose. Google "get docker" for your fix of Docker.
This has been tested on Hardkernel's Odroid XU-4, HC-2 (basically the same).
It will NOT work as is for Raspberry Pies >= 3 as these are a different processor architecture.


## The What
This is a small toolbox for creating a dockerised node-exporter which can be scraped from Prometheus or other tools. It exposes information via http:9100.   

The variant "single" is just the node-exporter running with a set of standard collectors. If you want to enable other/more/less collectors, check the official node-exporter documentation on what to addd to the compose file.   

The variant "odroid" is sepcifically created for odroid SBCs, for which it will also add information about CPU temperature. Actually we just parse one of the values (there are 4) but as the temp diodes are pretty close together you wouldn't see differences between all four of them. So we settled for just one temp diode.   
For temperature parsing, a cron-script, as well as an enabled text collector is required. This is a very lean and simple example how to make use of the text collector in node-exporter.   

## The How To
Steps to use the odroid variant:
- create crontab entry for cron-odroid-temp.sh for root similar to:
```*/5 * * * * /root/scripts/cron-odroid-temp.sh > /var/log/node-exporter/cputemp.prom```
(N.B.: the file must end in .prom else it won't be parsed!)
- run the container:
```docker-compose -f docker-compose-node-exporter-odroid-temp.yml```
- test should reveal a value for temperature at the end of metrics
```curl http://localhost:9100/metrics < grep -i temper```

This can be adapted widely to your needs. Check the official documentation for node-exporter and the text file collector.

## Building your own image
A working Dockerfile is part of this repository. Adapt this to your needs.
